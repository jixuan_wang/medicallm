import re, os
import numpy as np

corpus = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/clean_corpus.txt'
output = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/words.txt'
output_short = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/words_short.txt'

with open(corpus, 'r') as f:
    text = f.read() 

print ('Done reading file')

words = re.findall('\w+', text.lower())
uniq_words = list(set(words))

for i in range(len(uniq_words)):
    uniq_words[i] = uniq_words[i].upper()


uniq_words.sort()
print('Length of words scraped is ' + str(len(uniq_words)))
with open(output, 'w') as f:
    for u in uniq_words:
        if not u.isdigit():
            f.write(u.strip() + '\n')


selected = np.random.choice(len(uniq_words), 100)
with open(output_short, 'w') as f:
    for i in selected:
        u = uniq_words[i]
        if not u.isdigit():
            f.write(u.strip() + '\n')
