mkdir -p $HOME/pubmed_abstracts

for ((start=20000;start < 2095999;start+=125000))
{
    python scrape_pubmed.py $start >> $HOME/pubmed_abstracts/scrape_${start}.log 2>&1 &
}