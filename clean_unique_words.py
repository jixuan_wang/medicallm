import os
import pprint as pp

ORIGINAL = os.path.join(os.environ['HOME'], 'pubmed_stuff', 'pubmed_data', 'words.txt')
CLEAN = os.path.join(os.environ['HOME'], 'pubmed_stuff', 'pubmed_data', 'clean_words.txt')

result = []
rejected = 0
rejected_list = []
with open(ORIGINAL, 'r') as f:
    lines = f.readlines()

    for l in lines:

        can_append = True
        if l[0] == "'":
            can_append = False
        
        if can_append:
            l = l.strip()
            
            cap = round(len(l) * float(2) / 3)
            prev_char = '!' 
            repeated = 0
            for char in l:
                if char == prev_char:
                    repeated += 1

                    if repeated >= cap:
                        can_append = False
                        break
                else:
                    repeated = 1
                    prev_char = char
                

        if can_append:
            result.append(l)
        else:
            rejected += 1
            rejected_list.append(l.strip())

print('Rejected count: ' + str(rejected))
print('Accepted count: ' + str(len(result)))
if False:
    print(rejected_list)

with open(CLEAN, 'w') as f:
    for l in result:
        f.write(l + '\n')