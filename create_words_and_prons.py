import re, os

corpus = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/clean_corpus.txt'
output = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/words.txt'

with open(corpus, 'r') as f:
    text = f.read() 

words = re.findall('\w+', text.lower())
uniq_words = list(set(words))

for i in range(len(uniq_words)):
    uniq_words[i] = uniq_words[i].upper()


with open(output, 'w') as f:
    for u in uniq_words:
        f.write(u.strip() + '\n')
    