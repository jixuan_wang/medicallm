from Bio import Entrez
import os, sys

Entrez.email = 'your_email@provider.com'
batch_count = 1000

start = int(sys.argv[1])
end = start + 125000        # assume we have 16 of these guys

with open(os.environ['HOME'] + '/pubmed_stuff/pubmed_data/scraped/scraped_pubmed_' + str(start) + '.txt', 'a') as f:
    #for i in range(175400, 2895999, batch_count):
    for i in range(start, end, batch_count):
        print('==> Processing PMID starting ' + str(i))

        pmids = list(range(i, i + batch_count))

        try:
            handle = Entrez.efetch(db="pubmed", id=','.join(map(str, pmids)),
                                   rettype="xml", retmode="text")
            records = Entrez.read(handle)
        except:
            print('Error running entrez at ' + str(i))

        abstracts = []
        for c, pubmed_article in enumerate(records['PubmedArticle']):
            try:
                abstracts.append(pubmed_article['MedlineCitation']['Article']['Abstract']['AbstractText'][0])
            except:
                print('No abstract for ' + str(i + c))

        #abstract_dict = dict(zip(pmids, abstracts))
        #print(abstract_dict)

        for a in abstracts:
            content = a.strip().encode('ascii', 'ignore')
            f.write(content + '\n')