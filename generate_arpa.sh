corpus_source=$HOME/pubmed_stuff/pubmed_data/clean_corpus.txt
word_source=$HOME/pubmed_stuff/pubmed_data/words.txt
tool_source=$HOME/kaldi/tools/srilm/bin/i686-m64/ngram-count
output_lm=$HOME/pubmed_stuff/pubmed_data/lm.arpa
output_upper_corpus=$HOME/pubmed_stuff/pubmed_data/corpus_upper.txt

# Must convert courpus to upper case else SRILM will give strange stuff
echo "Converting corpus to uppercase"
cat $corpus_source | tr '[:lower:]' '[:upper:]' > $output_upper_corpus


echo "Processing n-gram probabilities"

# Seems like SRILM has issue with KN smoothing after we filter stuff from the language model
# This is because we removed all singletons effectively. KN smoothing needs words that occur once
$tool_source -text $output_upper_corpus -order 3 -limit-vocab -vocab \
    $word_source -unk -map-unk "<unk>" -kndiscount -interpolate -lm $output_lm

# I guess you could delete all 2,3 grams if you are only interested in 1-gram model
#$tool_source -text $corpus_source -order 3 -limit-vocab -vocab \
#    $word_source -map-unk "<unk>" -wbdiscount -interpolate -lm $output_lm