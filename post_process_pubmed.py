import os, sys, re 


start = int(sys.argv[1])
end = start + 125000        # assume we have 16 of these guys

english_dict = set()
#with open(os.environ['HOME'] + '/pubmed_stuff/english-words/words_alpha.txt', 'r') as f_dict:
with open(os.environ['HOME'] + '/pubmed_stuff/google-10000-english/20k.txt', 'r') as f_dict:
    lines = f_dict.readlines()
    for l in lines:
        word = l.strip()
        english_dict.add(word)

original =  os.environ['HOME'] + '/pubmed_stuff/pubmed_data/scraped/scraped_pubmed_' + str(start) + '.txt'
processed = os.environ['HOME'] + '/pubmed_stuff/pubmed_data/postprocessed/postprocessed_pubmed_' + str(start) + '.txt'


REGEX_2TO1 = re.compile(r'\s{2,}')
REGEX_HTML = re.compile(r'http:\S+\b|https:\S+\b|www\.\S+\b|www3\.\S+\b|ftp:\S+\b', flags=re.I | re.X)
REGEX_PUNCS = re.compile(r'[!"#$%&\(\)*\[\]+,-/:;<=>?@\\^_`{\|}~]')
REGEX_NUMS = re.compile(r'([0-9]+)')
REGEX_ABBREVS = re.compile(r'et\.al|et\sal\.|et\sal\s|et\sall\s|\b[A-Z]\.[A-Z]\b', flags=re.I | re.X)
REGEX_APOS = re.compile(r'[0-9A-Za-z]\'(?!\S)')
REGEX_APOS2 = re.compile(r'(?<=\s)\'')

def truncate_string(words):
    desired_sequences = []

    start = 0
    not_seen = 0
    contain = False
    for i, w in enumerate(words):

        if w not in english_dict and not w.isdigit():
            contain = True
            not_seen = 0
        else:
            not_seen += 1
            
            if not_seen >= 3:
                if contain:
                    desired = ' '.join(words[start:i])
                    desired_sequences.append(desired)   
                    contain = False
                not_seen = 1
                start = i - 1
                

    if contain:
        desired = ' '.join(words[start:i])
        desired_sequences.append(desired)
                    
    #pp.pprint(desired_sequences)
    return desired_sequences


def cleanup_string(string):
    
    string=string.encode("ascii","replace")

    string = REGEX_HTML.sub(' ', string)
    string = REGEX_ABBREVS.sub(' ', string)
    string = REGEX_PUNCS.sub(' ', string)
    string = REGEX_NUMS.sub(r' \1 ', string)
    string = REGEX_2TO1.sub(' ', string)
    string = REGEX_APOS.sub(' ', string)
    string = REGEX_APOS2.sub(' ', string)

    string = string.lower()

    desired_sequences = []
    splitted_lines = string.split('.')
    for l in splitted_lines:
        if len(l) > 100:
            words = l.split()

            desired = truncate_string(words)
            desired_sequences.extend(desired) 
                    
    return desired_sequences



with open(processed, 'w') as f_write:

    with open(original, 'r') as f_read:
        line = f_read.readline()
        line_count = 1
        
        while line:
            
            results = cleanup_string(line)

            for r in results:
                f_write.write(str(r) + '\n')


            line = f_read.readline()
            line_count = 1
