import os, urllib2, time
import cookielib
from HTMLParser import HTMLParser
from lxml import html
from lxml import etree
from timeout import timeout

#url = "http://www.infolanka.com/miyuru_gee/art/art.html"
#page = html.fromstring(urllib.urlopen(url).read())

cookiejar= cookielib.LWPCookieJar()
address = "https://www.emedicinehealth.com/script/main/art.asp?articlekey="

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}


hdr2 = [('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11'),
       ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
       ('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3'),
       ('Accept-Encoding', 'none'),
       ('Accept-Language', 'en-US,en;q=0.8'),
       ('Connection', 'keep-alive')]


def get_content(html):
#    print(page.text)
    
    results = []
    for stuff in html.xpath('//div[@class="article"]//p'):
        #print stuff.text
        #print (stuff.innerHTML)
        string = etree.tostring(stuff, pretty_print=True)
        
        string = string.replace("&#13", "")
        string = string.replace("<p>", "")
        string = string.replace("</p>", "")
        string = string.replace(";", "")
        string = string.strip()

        if len(string) > 10 and "<!--" not in string:
            #print(string)
            
            if "</b>" in string:
                strings = string.split("</b>")
                s1 = strings[0][3:]
                s2 = strings[1]
                
                results.append(s1)
                results.extend(s2.split("."))
            else:
                results.extend(string.split("."))

    return results



for i in range(3595, 40682):
    
    time.sleep(0.25)
        
#    i = 6374
    
    full_address = address + str(i)
    
    proxy_support = urllib2.ProxyHandler({})
    #req = urllib2.Request(site, headers=hdr)
    opener= urllib2.build_opener( proxy_support )
    opener.addheaders = hdr2
    
    with open('parsed_texts.txt', 'a') as f:

        try:
            print('Retrieving ' + full_address)
    #        a = urllib.urlopen(full_address).read()
    #        print(a)
            #opener= urllib2.build_opener( urllib2.HTTPCookieProcessor(cookiejar) )
            #a = opener.urlopen(full_address).read()
            #print(a)
            with timeout(seconds=5):
                a = opener.open(full_address).read()
                #print(a)
                #get_content(a)
                
                page = html.fromstring(a)
                parsed = get_content(page)
                
                for p in parsed:
                    p = p.strip()
                    if len(p) > 0 and p != "Source:":
                        f.write(p + '\n')


        except:
            #print(a)
            print('Could not parse: ' + full_address)
