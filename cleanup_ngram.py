import os, sys
import pprint as pp
import numpy as np

NGRAM_PATH = os.path.join(os.environ['HOME'], 'pubmed_stuff', 'pubmed_data', 'lm.arpa')
NGRAM_OUT_PATH = os.path.join(os.environ['HOME'], 'pubmed_stuff', 'pubmed_data', 'lm_clean.arpa')

#NGRAM_PATH = os.path.join(os.environ['HOME'], 'Desktop', 'test.arpa')
#NGRAM_OUT_PATH = os.path.join(os.environ['HOME'], 'Desktop', 'test_clean.arpa')

BI_CUTOFF_RATIO = 0.5
TRI_CUTOFF_RATIO = 0.5


def parse_line(line, n):
    # -2.27286        <unk>   -1.750456

    splits = line.split()
    prob = round(float(splits[0]), 7)
    phrase = ' '.join(splits[1:1 + n])
    if len(splits) > 1 + n:
        bin_prob = float(splits[-1])
    else:
        bin_prob = ''
    
    return [prob, phrase, bin_prob]


def read_arpa():

    uni_gram = []
    bi_gram = []
    tri_gram = []

    with open(NGRAM_PATH, 'r') as f:
        lines = f.readlines()
        
        status = 'init'
        for l in lines:

            if len(l.strip()) == 0:
                status = 'init'
            else:
                if status == '1':
                    uni_gram.append(parse_line(l.strip(), 1))
                if status == '2':
                    bi_gram.append(parse_line(l.strip(), 2))
                if status == '3':
                    tri_gram.append(parse_line(l.strip(), 3))

            if '1-grams' in l:
                status = '1'
            elif '2-grams' in l:
                status = '2'
            elif '3-grams' in l:
                status = '3'

    return np.array(uni_gram), np.array(bi_gram), np.array(tri_gram)


def output(uni_gram, bi_gram, tri_gram):
    '''
    \data\
    ngram 1=36135
    ngram 2=6515183
    ngram 3=11670273
    '''

    with open(NGRAM_OUT_PATH, 'w') as f:
        f.write('\\data\\\n')

        f.write('ngram 1=')
        f.write(str(len(uni_gram)))
        f.write('\n')

        f.write('ngram 2=')
        f.write(str(len(bi_gram)))
        f.write('\n')

        f.write('ngram 3=')
        f.write(str(len(tri_gram)))
        f.write('\n')

        f.write('\\1-grams:\n')
        for l in uni_gram:
            line = str(l[0]).ljust(12) + str(l[1]).ljust(max(24, len(l[1]) + 1)) + str(l[2])
            f.write(line + '\n')
        f.write('\n')
        f.write('\n')

        f.write('\\2-grams:\n')
        for l in bi_gram:
            line = str(l[0]).ljust(12) + str(l[1]).ljust(max(24, len(l[1]) + 1)) + str(l[2])
            f.write(line + '\n')
        f.write('\n')

        f.write('\\3-grams:\n')
        for l in tri_gram:
            line = str(l[0]).ljust(12) + str(l[1]).ljust(max(24, len(l[1]) + 1)) + str(l[2])
            f.write(line + '\n')
        f.write('\n')


def n_gram_filtering(bi_gram, tri_gram):
    #pp.pprint(bi_gram)
    sorted_bi = bi_gram[bi_gram[:,0].argsort()]
    sorted_tri = tri_gram[tri_gram[:,0].argsort()]
    #pp.pprint(sorted_bi)

    print('Filtering bi-grams')
    bi_leftover = np.array(sorted_bi[ : int(len(bi_gram) * BI_CUTOFF_RATIO)])
    #pp.pprint(bi_leftover)
    bi_result = bi_leftover[bi_leftover[:,1].argsort()]
    #pp.pprint(bi_result)


    print('Filtering tri-grams')
    tri_leftover = np.array(sorted_tri[ : int(len(tri_gram) * TRI_CUTOFF_RATIO)])
    tri_result = tri_leftover[tri_leftover[:,1].argsort()]

    return bi_result, tri_result


def main():
    uni_gram, bi_gram, tri_gram = read_arpa()

    bi_gram_filtered, tri_gram_filtered = n_gram_filtering(bi_gram, tri_gram)

    output(uni_gram, bi_gram_filtered, tri_gram_filtered)


if __name__ == '__main__':
    main()

        
    
