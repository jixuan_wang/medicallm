mkdir -p $HOME/pubmed_stuff/pubmed_data
mkdir -p $HOME/pubmed_stuff/pubmed_data/word_pron

top_dir=$HOME/pubmed_stuff/pubmed_data/
word_dir=$HOME/pubmed_stuff/pubmed_data/word_pron

echo "Spliting clean words into sub files"
split -l 2500 -d --additional-suffix=.txt ${top_dir}/words.txt ${word_dir}/word_pron_split_

echo "Generating pronounciation"
fillnames=""
for sub in "00" "01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12" "13" "14" "15" "16"
do
    echo "/usr/local/bin/g2p.py --model $HOME/pubmed_stuff/model-b.key --apply \
    ${word_dir}/word_pron_split_$sub.txt > ${word_dir}/words_${sub}.dic"
    
    /usr/local/bin/g2p.py --model $HOME/pubmed_stuff/model-b.key --apply \
    ${word_dir}/word_pron_split_$sub.txt > ${word_dir}/words_${sub}.dic &

    filenames="$filenames ${word_dir}/words_${sub}.dic"
done

FAIL=0

for job in `jobs -p`
do
echo $job
    wait $job || let "FAIL+=1"
done


echo "Combining pronounciation files"
cat $filenames > ${top_dir}/words.dic