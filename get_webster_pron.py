import requests
import re, os, io


from ipapy.arpabetmapper import ARPABETMapper
amapper = ARPABETMapper()

WEBSTER = 'https://www.merriam-webster.com/dictionary/'


OXFORD = 'https://en.oxforddictionaries.com/definition/'
REGEX_OX_PRON = re.compile(r'<span class="phoneticspelling">(?P<pron>.*?)</span>', flags=re.I)

#word_list = '20k.txt'
word_list = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/words.txt'
output = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/word_prons.txt'


def pronounce(word):
    response = requests.get(OXFORD + word)
    text = response.text

    pron = REGEX_OX_PRON.search(text)

    result = None
    if pron is not None:
        result = pron.groupdict()['pron']
        result = result.replace('/', '')
        result = result.replace('\'', '')
        
    return result


def convert_to_arpabet(pron):

    if pron is not None:
        result = amapper.map_unicode_string(pron, ignore=True, return_as_list=True)
        result_string = ' '.join(result)
        return result_string
    
    else:
        return None


with open(word_list, 'r') as f:
    with open(output, 'w') as outf:
        lines = f.readlines()
        for l in lines[:20]:
            l = l.strip()        

            pron = pronounce(l)
            arpa = convert_to_arpabet(pron)

            if pron is not None:
                out = l + ' ; ' + pron + ' ; ' + str(arpa)
            else:
                out = l + ' ; ' + str(None)

            print(out)
            outf.write(out)
            outf.flush()
