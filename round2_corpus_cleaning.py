import os

CLEAN_WORDS = os.path.join(os.environ['HOME'], 'pubmed_stuff', 'pubmed_data', 'clean_words.txt')
INITIAL_CORPUS = os.path.join(os.environ['HOME'], 'pubmed_stuff', 'pubmed_data', 'initial_corpus.txt')
CLEAN_CORPUS = os.path.join(os.environ['HOME'], 'pubmed_stuff', 'pubmed_data', 'clean_corpus.txt')

fancy_dict = set()
with open(CLEAN_WORDS, 'r') as f_dict:
    lines = f_dict.readlines()
    for l in lines:
        word = l.strip()
        fancy_dict.add(word.lower())


english_dict = set()
with open(os.environ['HOME'] + '/pubmed_stuff/google-10000-english/20k.txt', 'r') as f_dict:
    lines = f_dict.readlines()
    for l in lines:
        word = l.strip()
        english_dict.add(word.lower())


desired_lines = []
reasons_reject = set()
with open(INITIAL_CORPUS, 'r') as f:
    lines = f.readlines()

    for l in lines:
        splits = l.strip().split(' ')            

        # remove low frequency stuff
        keep = True
        for s in splits:
            if s not in fancy_dict and s not in english_dict and not s.isdigit():
                keep = False
                #print(l)
                #print(s)
                #exit()
                reasons_reject.add(s)
                break

        if keep:
            desired_lines.append(l)

print('accepted_lines: ' + str(len(desired_lines)))
print('total_lines:    ' + str(len(lines)))

if True:
    print(reasons_reject)

print('Writing result')

with open(CLEAN_CORPUS, 'w') as f:
    for d in desired_lines:
        f.write(d.strip() + '\n')

print('Done')

