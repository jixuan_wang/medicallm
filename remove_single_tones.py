from __future__ import division
import os, sys
import pprint as pp

# Only when we need to sort them
#for key, value in sorted(collected_words.iteritems(), key=lambda (k,v): (v,k)):
#    print("%s: %s" % (key, value))

REMOVE_THRESHOLD = 100
PATH = os.environ['HOME'] + '/pubmed_stuff/pubmed_data/postprocessed/'
OUTPATH = os.environ['HOME'] + '/pubmed_stuff/pubmed_data/'

collected_words = {}
collected_lines = []
total_word_length = 0
print ('Synthesis all sub-text files first')
for i in range(0, 2100000, 125000):
#for i in range(0, 125000, 125000):
    filename = os.path.join(PATH, 'postprocessed_pubmed_' + str(i) + '.txt')

    print('Synthesizing from ' + str(filename))
    sys.stdout.flush()

    with open(filename, 'r') as f:
        lines = f.readlines()

        for l in lines:
            splits = l.strip().split(' ')            
            total_word_length += len(splits)

            for s in splits:
                if s not in collected_words:
                    collected_words[s] = 1
                else:
                    collected_words[s] += 1

        collected_lines.extend(lines)

if True:
    sorted_words = sorted(collected_words.iteritems(), key=lambda (k,v): (v,k))
    #pp.pprint(sorted_words)
    #print(len(sorted_words))
    sys.stdout.flush()
    count = 0
    for s in sorted_words:
        if s[1] == 1:
            count += 1

    print('singleton: ' + str(count))

print ('Corpus has a total of ' + str(total_word_length) + ' words')
print ('Removing undesired sentences')

# Remove all sentences with stuff that only show up a few times
desired_lines = []
for i, l in enumerate(collected_lines):
    
    if (i + 1) % int(len(collected_lines) / 10) == 0:
        progress = (i + 1) * 100 / len(collected_lines)
        progress = round(progress, 1)
        print('Progress: ' + str(progress))

    splits = l.strip().split(' ')            

    # remove low frequency stuff
    keep = True
    for s in splits:
        if collected_words[s] < REMOVE_THRESHOLD:
            keep = False
            break
    
    # remove sentences made of numbers
    digit = 0
    cap = len(splits) / 3
    for s in splits:
        if s.isdigit():
            digit += 1
            if digit > cap:
                keep = False
                break
    
    if keep:
        desired_lines.append(l)


print('desired_lines:   ' + str(len(desired_lines)))
print('collected_lines: ' + str(len(collected_lines)))

print('Writing result')
output_name = os.path.join(OUTPATH, 'initial_corpus.txt')
with open(output_name, 'w') as f:
    for d in desired_lines:
        f.write(d.strip() + '\n')

print('Done')






