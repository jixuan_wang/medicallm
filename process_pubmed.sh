mkdir -p $HOME/pubmed_abstracts

for ((start=20000;start < 2095999;start+=125000))
{
    python post_process_pubmed.py $start >> $HOME/pubmed_abstracts/scrape_post_${start}.log 2>&1 &
}