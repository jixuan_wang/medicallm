import pycurl, os
import cStringIO

class FileReader:
    def __init__(self, fp):
        self.fp = fp
    def read_callback(self, size):
        return self.fp.read(size)

buf = cStringIO.StringIO()

c = pycurl.Curl()
c.setopt(c.URL, 'http://35.183.3.64:8888/client/dynamic/recognize')
f = open('peterson_5sec.wav', 'rb')
c.setopt(c.UPLOAD, 1)
c.setopt(pycurl.READFUNCTION, FileReader(open('peterson_5sec.wav', 'rb')).read_callback)

filesize = os.path.getsize('peterson_5sec.wav')
c.setopt(pycurl.INFILESIZE, filesize)

c.perform()

print buf.getvalue()
buf.close()
