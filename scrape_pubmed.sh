mkdir -p $HOME/pubmed_stuff/pubmed_data
mkdir -p $HOME/pubmed_stuff/pubmed_data/scraped

FAIL=0

for ((start=0;start < 2095999;start+=125000))
{
    echo "(Scrape PubMed) Starting " $start
    python scrape_pubmed.py $start >> $HOME/pubmed_stuff/pubmed_data/scraped/scrape_pubmed_${start}.log 2>&1 &
}

for job in `jobs -p`
do
echo $job
    wait $job || let "FAIL+=1"
done

if [ "$FAIL" == "0" ];
then
echo "DONE!"
else
echo "FAIL! ($FAIL)"
fi