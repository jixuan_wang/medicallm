# YOU MUST RUN THIS FILE USING PYTHON3
# PYTHON2 HAS WERID ISSUES WITH UNICODE, TOO LAZY TO FIGURE OUT

import requests
import re, os, io
import pprint as pp
from ipapy.arpabetmapper import ARPABETMapper

amapper = ARPABETMapper()

CMU_ALPHABET = set(['AA', 'AE', 'AH', 'AO', 'AW', 'AY',
                'B',
                'CH',
                'D', 'DH',
                'EH', 'ER', 'EY',
                'F',
                'G',
                'HH',
                'IH', 'IY',
                'JH',
                'K',
                'L',
                'M',
                'N', 'NG',
                'OW', 'OY',
                'P',
                'R',
                'S', 'SH',
                'T', 'TH',
                'UH', 'UW',
                'V',
                'W',
                'Y',
                'Z',
                'ZH'])

UNKNOWN_TO_CMU_MAP = {'AX': 'AH', 'UX': 'UW'}
IPA_TO_CMU_MAP = {'ē': 'IY', 'ä': 'AH', 
                    'ī': 'AY', 'ɒ': 'AH',
                    'ō': 'OW', 'ȯ': 'AO',
                    'ā': 'AY', 'ᵊ': 'UH',
                    'y': 'IY', 'c': 'CH'}


WEBSTER = 'https://www.merriam-webster.com/dictionary/'
REGEX_MW_PRON = re.compile(r'<span class="mw">(?P<pron>.*?)</span>', flags=re.I)

OXFORD = 'https://en.oxforddictionaries.com/definition/'
REGEX_OX_PRON = re.compile(r'<span class="phoneticspelling">(?P<pron>.*?)</span>', flags=re.I)

REGEX_REMOVE_BRACKET = re.compile(r'\(.*?\)', flags=re.I)


#word_list = '20k.txt'
word_list = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/words.txt'
#word_list = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/words_short.txt'
output = os.environ['HOME'] + '/pubmed_stuff/pubmed_abstracts/word_prons.txt'

unknown_arpa = set()
unknown_ipa = set()

def pronounce_oxford(word):
    response = requests.get(OXFORD + word)
    text = response.text

    pron = REGEX_OX_PRON.search(text)

    if pron is not None:
        pron = pron.groupdict()['pron']
        
    return pron


def pronounce_mw(word):
    response = requests.get(WEBSTER + word)
    text = response.text
    #print(text)

    pron = REGEX_MW_PRON.search(text)

    if pron is not None:
        pron = pron.groupdict()['pron']
        
    return pron


def cleanup_pron(pron):
    
    result = None
    if pron is not None:
        result = pron.replace('/', '')
        result = result.replace('ˈ', '')
        result = result.replace('ˌ', '')
        result = result.replace('-', '')
        result = REGEX_REMOVE_BRACKET.sub('', result)

        result = result.replace('ː', '')
        result = result.replace('¦', '')
        result = result.replace(',', '')
    
    return result



def identify_problem_ipa(pron):
    for p in pron:
        try:
            result = amapper.map_unicode_string(p, ignore=False, return_as_list=True)
        except:
            unknown_ipa.add(p)


def custom_map(pron):
    result = []
    for p in pron:
        try:
            temp = amapper.map_unicode_string(p, ignore=False, return_as_list=True)[0]
        except:
            
            if p in IPA_TO_CMU_MAP:
                temp = IPA_TO_CMU_MAP[p]
            else:
                unknown_ipa.add(p)
        
        result.append(temp)
        
    return result


def convert_to_arpabet(pron):

    if pron is not None:
        
        has_issue = False

        try:
            result = amapper.map_unicode_string(pron, ignore=False, return_as_list=True)
        except:
            result = amapper.map_unicode_string(pron, ignore=True, return_as_list=True)
            #identify_problem_ipa(pron)

            try:
                result = custom_map(pron)
            except:
                has_issue = True

        for i, r in enumerate(result):
            if r not in CMU_ALPHABET:
                if r not in UNKNOWN_TO_CMU_MAP:
                    unknown_arpa.add(r)
                else:
                    result[i] = UNKNOWN_TO_CMU_MAP[r]

        result_string = ' '.join(result)
        return result_string, has_issue
    
    else:
        return None, True


def workflow():

    with open(word_list, 'r') as f:
        with open(output, 'w') as outf:
            lines = f.readlines()
            for l in lines:
                l = l.strip()        

                pron_ox = pronounce_oxford(l)
                pron_ox_clean = cleanup_pron(pron_ox)
                arpa_ox, issue_ox = convert_to_arpabet(pron_ox_clean)

                pron_mw = pronounce_mw(l)
                pron_mw_clean = cleanup_pron(pron_mw)
                arpa_mw, issue_mw = convert_to_arpabet(pron_mw_clean)

                out = l.ljust(15) + ' ; '

                if pron_ox is not None:
                    out += pron_ox.ljust(20) + ' ; ' + arpa_ox.ljust(25) + ' ; ' + str(issue_ox).ljust(8) + ' ; \n'
                else:
                    out += 'None'.ljust(20) + ' ; ' + 'None'.ljust(25) + ' ; ' + '-'.ljust(8) + ' ; \n'

                out += ' '.ljust(15) + ' ; '
                if pron_mw is not None:
                    out += pron_mw.ljust(20) + ' ; ' + arpa_mw.ljust(25) + ' ; ' + str(issue_mw).ljust(8) + ' ; '
                else:
                    out += 'None'.ljust(20) + ' ; ' + 'None'.ljust(25) + ' ; ' + '-'.ljust(8) + ' ; '

                print(out)
                outf.write(out)
                outf.flush()



if __name__ == '__main__':
    
    workflow()

    pp.pprint(unknown_arpa)
    pp.pprint(unknown_ipa)

    exit()

    word = 'macrocephaly'
    #ox_pron = pronounce_oxford(word)
    #print(ox_pron)

    mw_pron = pronounce_mw(word)
    print(mw_pron)