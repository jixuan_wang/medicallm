mkdir -p $HOME/pubmed_stuff/pubmed_data
mkdir -p $HOME/pubmed_stuff/pubmed_data/postprocessed

FAIL=0

for ((start=0;start < 2095999;start+=125000))
{
    echo "(Postprocess PubMed) Starting " $start
    python post_process_pubmed.py $start >> \
        $HOME/pubmed_stuff/pubmed_data/postprocessed/postprocess_pubmed_${start}.log 2>&1 &
}

for job in `jobs -p`
do
echo $job
    wait $job || let "FAIL+=1"
done

if [ "$FAIL" == "0" ];
then
echo "DONE!"
else
echo "FAIL! ($FAIL)"
fi