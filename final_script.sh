# Set up the environment variables (again)
. cmd.sh
. path.sh
 
# Set the paths of our input files into variables
echo "Set the paths of our input files into variables"

model=exp/tdnn_7b_chain_online
phones_src=exp/tdnn_7b_chain_online/phones.txt
dict_src=new/local/dict
lm_src=new/local/lang/lm.arpa
 
lang=new/lang
dict=new/dict
dict_tmp=new/dict_tmp
graph=new/graph
 
# Compile the word lexicon (L.fst)
echo "Compile the word lexicon (L.fst)"
utils/prepare_lang.sh --phone-symbol-table $phones_src $dict_src "<unk>" $dict_tmp $dict
 
# Compile the grammar/language model (G.fst)
echo "Compile the grammar/language model (G.fst)"
gzip < $lm_src > $lm_src.gz
utils/format_lm.sh $dict $lm_src.gz $dict_src/lexicon.txt $lang
 
# Finally assemble the HCLG graph
echo "Finally assemble the HCLG graph"
utils/mkgraph.sh --self-loop-scale 1.0 $lang $model $graph
 
# To use our newly created model, we must also build a decoding configuration, the following line will create these for us into the new/conf directory
echo "# To use our newly created model, we must also build a decoding configuration, the following line will create these for us into the new/conf directory"
steps/online/nnet3/prepare_online_decoding.sh --mfcc-config conf/mfcc_hires.conf $dict exp/nnet3/extractor exp/chain/tdnn_7b new