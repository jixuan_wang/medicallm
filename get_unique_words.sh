# $1 = output folder 
# $2 = name of clean corpus, something like corpus.txt

grep -oE "[A-Za-z\\-\\']{3,}" $1/$2 | tr '[:lower:]' '[:upper:]' | sort | uniq > $1/words.txt